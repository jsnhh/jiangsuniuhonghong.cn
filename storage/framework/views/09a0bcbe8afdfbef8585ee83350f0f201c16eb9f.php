<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>交易返现规则</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
    <style>
        .edit {
            background-color: #ed9c3a;
        }

        .shenhe {
            background-color: #429488;
        }

        .see {
            background-color: #7cb717;
        }

        .tongbu {
            background-color: #4c9ef8;
            color: #fff;
        }

        .cur {
            color: #009688;
        }

        .storebox {
            height: 200px;
            overflow-y: auto;
            z-index: 999;
            position: absolute;
            left: 0px;
            top: 42px;
            width: 298px;
            background-color: #ffffff;
            border: 1px solid #ddd;
        }

        .storebox .list {
            height: 38px;
            line-height: 38px;
            cursor: pointer;
            padding-left: 10px;
        }

        .storebox .list:hover {
            background-color: #eeeeee;
        }
    </style>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12" style="margin-top:0px">

            <div class="layui-fluid">
                <div class="layui-row layui-col-space15">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header">交易返现规则</div>

                            <div class="layui-card-body">
                                <div class="layui-btn-container" style="font-size:14px;">
                                    <a class="layui-btn layui-btn-primary" lay-href="<?php echo e(url('/user/addCashBackRule')); ?>"
                                       style="background-color:#3475c3;border-radius: 5px;border:none;color:#fff;display: block;width: 122px;">添加返现规则</a>
                                    <!-- 选择设备 -->
                                    <div class="layui-form" lay-filter="component-form-group"
                                         style="width:300px;display: inline-block;">
                                        <div class="layui-form-item">
                                            <div class="layui-input-block" style="margin-left:0">
                                                <select name="rules" id="rules" lay-filter="rules" lay-search>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <table class="layui-hide" id="test-table-page" lay-filter="test-table-page"></table>
                                <!-- 判断状态 -->
                                <script type="text/html" id="table-content-list" class="layui-btn-small">
                                    <a class="layui-btn layui-btn-normal layui-btn-xs details"
                                       lay-event="details">详情</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs jc" lay-event="jc">检测</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs del" lay-event="del">删除</a>
                                    <a class="layui-btn layui-btn-normal layui-btn-xs edit" lay-event="edit">编辑</a>
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<input type="hidden" class="rule_id" value="">
<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index', 'form', 'table', 'laydate'], function () {
        var $ = layui.$
            , admin = layui.admin
            , form = layui.form
            , table = layui.table
            , laydate = layui.laydate;
        // 未登录,跳转登录页面
        $(document).ready(function () {
            if (token == null) {
                window.location.href = "<?php echo e(url('/user/login')); ?>";
            }
        })

        // 渲染表格
        table.render({
            elem: '#test-table-page'
            , url: "<?php echo e(url('/api/user/ruleList')); ?>"
            , method: 'post'
            , where: {
                token: token
            }
            , request: {
                pageName: 'p',
                limitName: 'l'
            }
            , page: true
            , cellMinWidth: 150
            , cols: [[
                {field: 'rule_name', align: 'center', title: '活动名称', width: 250}
                , {field: 'start_time', align: 'center', title: '开始时间'}
                , {field: 'end_time', align: 'center', title: '结束时间'}
                , {field: 'standard_amt', align: 'center', title: '激活奖励金额'}
                , {field: 'standard_term', align: 'center', title: '激活标准期限'}
                , {field: 'standard_satisfy_total', align: 'center', title: '激活达标总数量'}
                , {field: 'first_amt', align: 'center', title: '首月奖励金额'}
                , {field: 'first_satisfy_total', align: 'center', title: '首月达标总数量'}
                , {field: 'again_amt', align: 'center', title: '次月奖励金额'}
                , {field: 'again_satisfy_total', align: 'center', title: '次月达标总数量'}
                , {width: 200, align: 'center', fixed: 'right', toolbar: '#table-content-list', title: '操作'}
            ]]
            , response: {
                statusName: 'status' //数据状态的字段名称，默认：code
                , statusCode: 1 //成功的状态码，默认：0
                , msgName: 'message' //状态信息的字段名称，默认：msg
                , countName: 't' //数据总数的字段名称，默认：count
                , dataName: 'data' //数据列表的字段名称，默认：data
            }
            , done: function (res, curr, count) {
                $('th').css({
                    'font-weight': 'bold',
                    'font-size': '15',
                    'color': 'black',
                    'background': 'linear-gradient(#f2f2f2,#cfcfcf)'
                });	//进行表头样式设置
            }

        });
        getSelRule();
        function getSelRule(){
            // 选择员工
            $.ajax({
                url: "<?php echo e(url('/api/user/selRule')); ?>",
                type: 'get',
                data : {token:token},
                success: function (data) {
                    //console.log(data);
                    var optionStr = "";
                    for (var i = 0; i < data.data.length; i++) {
                        optionStr += "<option value='" + data.data[i].id + "'>" + data.data[i].rule_name + "</option>";
                    }
                    $("#rules").append('<option value="">选择规则</option>'+optionStr);
                    layui.form.render('select');
                },
                error: function (data) {
                    alert('查找板块报错');
                }
            });
        }
        //选择活动名称
        form.on('select(rules)', function (data) {
            category = data.value;
            //执行重载
            table.reload('test-table-page', {
                where: {
                    id: category
                }
                , page: {
                    curr: 1
                }
            });
        });
        table.on('tool(test-table-page)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
            var e = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr; //获得当前行 tr 的DOM对象
            //console.log(e);
            // sessionStorage.setItem('s_store_id', e.store_id);

            if (layEvent === 'del') {
                layer.confirm('确认删除此消息?', {icon: 2}, function (index) {
                    $.post("<?php echo e(url('/api/user/del_cash_back_rule')); ?>",
                        {
                            token: token, id: e.id

                        }, function (res) {
                            //console.log(res);
                            if (res.status == 1) {
                                obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                layer.close(index);
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 1
                                    , time: 2000
                                });
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 2
                                    , time: 3000
                                });
                            }
                        }, "json");

                });
            }
            if (layEvent === 'jc') {

                $.post("<?php echo e(url('/api/user/testing_standard')); ?>",
                    {
                        token: token,
                        id:e.id
                    }, function (res) {
                        //console.log(res);
                        if (res.status == 1) {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 1
                                , time: 2000
                            });
                            table.reload('test-table-page', {
                                page: {
                                    curr: 1
                                }
                            });

                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            }
            if (layEvent === 'details') {
                sessionStorage.setItem('terminalRewardId', e.id);
                sessionStorage.setItem('standard_term', 30);
                $('.details').attr('lay-href', "<?php echo e(url('/user/detailsCashBackRule')); ?>");

            }
            if (layEvent === 'edit') {
                sessionStorage.setItem('terminalRewardId', e.id);

                $('.edit').attr('lay-href', "<?php echo e(url('/user/editCashBackRule')); ?>");

            }


        });


    });

</script>

</body>
</html>





