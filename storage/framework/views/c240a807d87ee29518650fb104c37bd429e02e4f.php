<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>添加交易规则</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card" style="height: 250px">
                <div class="layui-card-body">
                    <div class="layui-form" lay-filter="component-form-group"
                         style="width:500px;display: inline-block;">
                        <div class="layui-form-item">
                            <label class="layui-form-label">代理商名称</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input agent_name"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="layui-form-item rule" style="display: none">
                            <label class="layui-form-label">返现规则</label>
                            <div class="layui-input-block">
                                <input type="text" autocomplete="off" class="layui-input rule_name"
                                       disabled="disabled">
                            </div>
                        </div>
                        <div class="layui-form-item selectRule" style="display: none">
                            <div class="layui-form-item selectRule" style="display: none">
                                <label class="layui-form-label">返现规则</label>
                                <div class="layui-input-block">
                                    <select name="rules" id="rules" lay-filter="rules" lay-search>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block" style="margin-left:0;text-align: center;margin-top:0px;">
                            <button class="layui-btn open" lay-submit="" style="display: none">确定</button>
                            <button class="layui-btn close" lay-submit=""
                                    style="background-color: #942a25;display: none">
                                停用
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" class="agent_id">
    <input type="hidden" class="rule_id">
    <script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
    <script>
        var token = sessionStorage.getItem("Usertoken");
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        layui.config({
            base: '../../../layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'form'], function () {
            var $ = layui.$
                , admin = layui.admin
                , element = layui.element
                , form = layui.form;
            var permission_idArr = [];
            form.render();
            // 未登录,跳转登录页面
            $(document).ready(function () {
                if (token == null) {
                    window.location.href = "<?php echo e(url('/user/login')); ?>";
                }
            });
            var id = getUrlParam('customer_id')
            var customer_name = decodeURI(escape(getUrlParam('customer_name')))
            $('.agent_name').val(customer_name)
            $('.agent_id').val(id)

            //通过代理商id获取规则
            getUserRule();
            // 规则列表
            getSelRule();


            function getSelRule() {

                $.ajax({
                    url: "<?php echo e(url('/api/user/selRule')); ?>",
                    type: 'get',
                    data: {token: token},
                    success: function (data) {
                        //console.log(data);
                        var optionStr = "";
                        for (var i = 0; i < data.data.length; i++) {
                            optionStr += "<option value='" + data.data[i].id + "'>" + data.data[i].rule_name + "</option>";
                        }
                        $("#rules").append('<option value="">选择规则</option>' + optionStr);
                        layui.form.render('select');
                    },
                    error: function (data) {
                        alert('查找板块报错');
                    }
                });
            }

            function getUserRule() {
                var user_id = $('.agent_id').val()

                $.ajax({
                    url: "<?php echo e(url('/api/user/get_user_rule')); ?>",
                    type: 'get',
                    data: {token: token, user_id: user_id},
                    success: function (data) {
                        if (data.data!=null&&data.data.status == "1") {

                            $('.close').show()
                            $('.open').hide()
                            $('.rule').show()
                            $('.selectRule').hide()
                            $('.rule_name').val(data.data.rule_name)
                            $('.rule_id').val(data.data.rule_id)
                        } else {
                            $('.close').hide()
                            $('.open').show()
                            $('.rule').hide()
                            $('.selectRule').show()
                        }
                    },
                    error: function (data) {

                    }
                });
            }
            //选择活动名称
            form.on('select(rules)', function (data) {
                category = data.value;
                $('.rule_id').val(category)
            });
            //开启规则
            $('.open').click(function () {
                var rule_id = $('.rule_id').val()
                if (rule_id == "") {
                    layer.alert("请选择返现规则");
                } else {
                    $.post("<?php echo e(url('/api/user/add_user_rule')); ?>",
                        {
                            token: token,
                            rule_id: rule_id,
                            user_id: $('.agent_id').val(),
                            user_name: $('.agent_name').val()
                        }, function (res) {
                            if (res.status == 1) {

                                parent.layer.msg(res.message, {
                                    icon: 1
                                    , time: 3000
                                });
                                parent.layer.close(index); //再执行关闭
                            } else {
                                layer.msg(res.message, {
                                    offset: '50px'
                                    , icon: 2
                                    , time: 3000
                                });
                            }
                        }, "json");
                }

            })

            //开启规则
            $('.close').click(function () {
                $.post("<?php echo e(url('/api/user/get_store_transaction')); ?>",
                    {
                        token: token,
                        agent_id: $('.agent_id').val()
                    }, function (res) {
                        if (res.status == 1) {
                            closeRule()
                        } else if (res.status == 3) {
                            layer.confirm('该规则已产生商户检测，是否确认停用?', {icon: 2}, function (index) {
                                closeRule()
                            });
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            })

            function closeRule() {
                $.post("<?php echo e(url('/api/user/close_user_rule')); ?>",
                    {
                        token: token,
                        agent_id: $('.agent_id').val(),
                    }, function (res) {
                        if (res.status == 1) {
                            parent.layer.msg(res.message, {
                                icon: 1
                                , time: 3000
                            });
                            parent.layer.close(index); //再执行关闭
                        } else {
                            layer.msg(res.message, {
                                offset: '50px'
                                , icon: 2
                                , time: 3000
                            });
                        }
                    }, "json");
            }

            //获取url中的参数
            function getUrlParam(name) {
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
                var r = window.location.search.substr(1).match(reg);  //匹配目标参数
                if (r != null) return unescape(r[2]);
                return null; //返回参数值
            }
        });
    </script>
</body>
</html>