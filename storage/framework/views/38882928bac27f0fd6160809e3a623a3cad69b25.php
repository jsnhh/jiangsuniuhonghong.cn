<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>设置比例</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
    <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
</head>
<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">设置比例</div>
        <div class="layui-card-body" style="padding: 15px;">
            <div class="layui-form" lay-filter="component-form-group">

                <div class="layui-form-item">
                    <label class="layui-form-label">比例上限</label>
                    <div class="layui-input-block">
                        <input type="text" name="p_profit" lay-verify="p_profit" autocomplete="off" placeholder="比例上限" class="layui-input name p_profit" disabled="true">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">比例%</label>
                    <div class="layui-input-block">
                        <input type="number" maxlength="3" name="profit" lay-verify="profit" autocomplete="off" placeholder="请输入比例" class="layui-input name profit">
                    </div>
                </div>

                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button class="layui-btn submit">确定提交</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="level" value="">

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var s_code = sessionStorage.getItem("s_code");
    var user_id = sessionStorage.getItem("dataid");
    var dataPid = sessionStorage.getItem("datapid");

    layui.config({
        base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index', //主入口模块
        formSelects: 'formSelects'
    }).use(['index', 'form','upload','formSelects'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,element = layui.element;

        // 未登录,跳转登录页面
        $(document).ready(function(){
            if(token==null){
                window.location.href="<?php echo e(url('/user/login')); ?>";
            }
        });

        $.post("<?php echo e(url('/api/user/getRateProfit')); ?>",
            {
                token:token,
                p_id:dataPid,
                user_id:user_id
            },function(res){
                if(res.status == 1){
                    $('.p_profit').val(res.data.p_profit);
                    $('.profit').val(res.data.profit);
                    $('.level').val(res.data.level);
                    var level = res.data.level;
                }

            },"json")

        $('.submit').on('click', function(){
            if(dataPid== 1){ //顶级代理
                var level = 1;
            }

            $.post("<?php echo e(url('/api/user/editRateProfit')); ?>",
                {
                    token:token,
                    p_id:dataPid,
                    user_id:user_id,
                    level:level,
                    p_profit: $('.p_profit').val(),
                    profit: $('.profit').val(),

                },function(res){
                    console.log(res);
                    if(res.status==1){
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 1
                            ,time: 1000
                        });
                    }else{
                        layer.msg(res.message, {
                            offset: '50px'
                            ,icon: 2
                            ,time: 1000
                        });
                    }
                },"json")


        });
    });
</script>
</body>
</html>
