<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>对账统计</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/layui/css/layui.css')); ?>" media="all">
  <link rel="stylesheet" href="<?php echo e(asset('/layuiadmin/style/admin.css')); ?>" media="all">
  <style>

    .userbox,.storebox{
      height:200px;
      overflow-y: auto;
      z-index: 999;
      position: absolute;
      left: 0px;
      top: 62px;
      width:298px;
      background-color:#ffffff;
      border: 1px solid #ddd;
    }
    .userbox .list,.storebox .list{
      height:38px;line-height: 38px;cursor:pointer;
      padding-left:10px;
    }
    .userbox .list:hover,.storebox .list:hover{
      background-color:#eeeeee;
    }
  </style>
</head>
<body>

  <div class="layui-fluid">
    <!-- 筛选------------------------------------------------------------ -->
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12" style="margin-top:0px;">
        <div class="layui-card">
          <div class="layui-card-header a_name">对账统计</div>

          <div class="layui-card-body">
            <div class="layui-btn-container" style="font-size:14px;">
              <!-- 选择业务员 -->
              <!-- <div class="layui-form" lay-filter="component-form-group" style="width:200px;display: inline-block;">
                <div class="layui-form-item">
                  <div class="layui-input-block" style="margin-left:10px;">
                      <select name="agent" id="agent" lay-filter="agent" lay-search>

                      </select>
                  </div>
                </div>
              </div> -->
              <div class="layui-form" lay-filter="component-form-group" style="width:200px;display: inline-block;">
                <div class="layui-form-item">
                  <div class="layui-input-block" style="margin-left:10px;">
                  <text class="yname">代理商名称</text>
                    <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入代理商名称" class="layui-input transfer">

                    <div class="userbox" style='display: none'></div>
                  </div>
                </div>
              </div>
              <!-- 学校 -->
              <!-- <div class="layui-form" lay-filter="component-form-group" style="width:200px;display: inline-block;">
                <div class="layui-form-item">
                  <div class="layui-input-block" style="margin-left:10px;">
                      <select name="schooltype" id="schooltype" lay-filter="schooltype" lay-search>

                      </select>
                  </div>
                </div>
              </div> -->
              <div class="layui-form" lay-filter="component-form-group" style="width:200px;display: inline-block;">
                <div class="layui-form-item">
                  <div class="layui-input-block" style="margin-left:10px;">
                  <text class="yname">门店名称</text>
                    <input type="text" name="schoolname" lay-verify="schoolname" autocomplete="off" placeholder="请输入门店名称" class="layui-input inputstore">

                    <div class="storebox" style='display: none'></div>
                  </div>
                </div>
              </div>
              <!-- 支付状态 -->
              <div class="layui-form" lay-filter="component-form-group" style="width:200px;display: inline-block;">
                <div class="layui-form-item">
                  <div class="layui-input-block" style="margin-left:10px;">
                  <text class="yname">支付类型</text>
                      <select name="status" id="status" lay-filter="status">
                        <option value="">全部</option>
                        <option value="alipay">支付宝</option>
                        <option value="weixin">微信</option>
                        <option value="alipay_face">支付宝刷脸</option>
                        <option value="weixin_face">微信刷脸</option>
                        <option value="member">会员支付</option>
                        <option value="jd">京东</option>
                        <option value="unionpay">银联刷卡</option>
                        <option value="unionpayqr">银联扫码</option>
                        <option value="unionpaysf">银联闪付</option>
                      </select>
                  </div>
                </div>
              </div>
              <!-- 通道类型 -->
              <div class="layui-form" lay-filter="component-form-group" style="width:200px;display: inline-block;">
                <div class="layui-form-item">
                  <div class="layui-input-block" style="margin-left:10px;">
                  <text class="yname">通道类型</text>
                      <select name="passway" id="passway" lay-filter="passway">

                      </select>
                  </div>
                </div>
              </div>

              <!-- 缴费时间 -->
              <div class="layui-form" style="display: inline-block;">
                <div class="layui-form-item" style='margin-left:10px;'>
                  <div class="layui-inline">

                    <div class="layui-input-inline" style='width:180px;'>
                    <text class="yname">订单开始时间</text>
                      <input type="text" class="layui-input start-item test-item" placeholder="订单开始时间" lay-key="23">
                      </div>
                    </div>
                    <div class="layui-inline">
                      <div class="layui-input-inline" style='width:180px;'>
                      <text class="yname">订单结束时间</text>
                        <input type="text" class="layui-input end-item test-item" placeholder="订单结束时间" lay-key="24">
                      </div>
                    </div>

                    <div class="layui-inline" style='margin-right:0'>
                      <div class="layui-input-inline" style='width:180px;'>
                      <text class="yname">订单金额</text>
                        <input type="text" class="layui-input amount_start" placeholder="订单金额">
                      </div>
                    </div>
                    -
                    <div class="layui-inline"  style='margin-left:10px;'>
                      <div class="layui-input-inline" style='width:180px;'>
                      <text class="yname">订单金额</text>
                        <input type="text" class="layui-input amount_end" placeholder="订单金额">
                      </div>
                    </div>

                </div>
              </div>



              <div class="layui-form" lay-filter="component-form-group" style="width:200px;display: inline-block;">
                <div class="layui-form-item">
                  <div class="layui-input-block" style="margin-left:10px;">
                     <button class="layui-btn" style="border-radius:5px;margin-top:15px;margin-bottom:0;" id="today">今日</button>
                     <button class="layui-btn" style="border-radius:5px;margin-top:15px;margin-bottom:0;" id="yesterday">昨日</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



    <!-- 统计数据------------------------------------------------------------ -->
    <div class="layui-row layui-col-space15">
      <div class="layui-col-md12">
        <div class="layui-card" style="background-color: transparent;">
          <div class="layui-card-header">统计数据(*商家实收=交易金额-退款金额-优惠金额，实际净额=商家实收-结算手续费)</div>
        </div>
      </div>
    </div>
    <div class="layui-row layui-col-space15"style="padding:6px">
      <div class="layui-col-sm6 layui-col-md4"style="width:26%">
      <div class="layui-card"style="background-color: #dbf9ff; border: 1px #ade0ff solid;border-radius: 5px;">
      <div class="layui-card-header"style="border-bottom:none">
            商家实收
            <span class="layui-badge layui-bg-blue layuiadmin-badge">元</span>
          </div>
          <div class="layui-card-body layuiadmin-card-list">
            <p class="layuiadmin-big-font acounts"style="font-size:25px;color:#595959"></p>
          </div>
        </div>
      </div>
      <div class="layui-col-sm6 layui-col-md4"style="width:26%">
        <div class="layui-card"style="margin-right:5px;background-color:#cffff1;border:1px #88f3c6 solid;border-radius: 5px;">
          <div class="layui-card-header"style="border-bottom:none">
            优惠金额
            <span class="layui-badge layui-bg-black layuiadmin-badge">元</span>
          </div>
          <div class="layui-card-body layuiadmin-card-list">
            <p class="layuiadmin-big-font acounts"style="font-size:25px;color:#595959"></p>
          </div>
        </div>
      </div>
      <div class="layui-col-sm6 layui-col-md4"style="width:26%">
        <div class="layui-card"style="margin-right:5px;background-color:#fbefe8;border:1px #f5c1a3 solid;border-radius: 5px;">
          <div class="layui-card-header"style="border-bottom:none">
            交易金额/笔数
            <span class="layui-badge layui-bg-orange layuiadmin-badge">元/笔</span>
          </div>
          <div class="layui-card-body layuiadmin-card-list">
            <p class="layuiadmin-big-font acounts"style="font-size:25px;color:#595959"></p>
          </div>
        </div>
      </div>

      <div class="layui-col-sm6 layui-col-md4"style="width:26%">
        <div class="layui-card"style="background-color:#fffafe;border:1px #ffbdfa solid;border-radius: 5px;">
          <div class="layui-card-header"style="border-bottom:none">
            退款金额/笔数
            <span class="layui-badge layuiadmin-badge" style="background-color: #5FB878;color: #fff;">元/笔</span>
          </div>
          <div class="layui-card-body layuiadmin-card-list">
            <p class="layuiadmin-big-font acounts"style="font-size:25px;color:#595959"></p>
          </div>
        </div>
      </div>
      <div class="layui-col-sm6 layui-col-md4"style="width:26%">
        <div class="layui-card"style="margin-right:5px;background-color:#ffe5f4;border:1px #f191b9 solid;border-radius: 5px;">
          <div class="layui-card-header"style="border-bottom:none">
            手续费
            <span class="layui-badge layui-bg-red layuiadmin-badge">元</span>
          </div>
          <div class="layui-card-body layuiadmin-card-list">
            <p class="layuiadmin-big-font acounts"style="font-size:25px;color:#595959"></p>
          </div>
        </div>
      </div>

        <div class="layui-col-sm6 layui-col-md4"style="width:26%">
            <div class="layui-card"style="margin-right:5px;background-color:#d9eeff;border:1px #95bbf4 solid;border-radius: 5px;">
                <div class="layui-card-header"style="border-bottom:none">
                    会员支付金额
                    <span class="layui-badge layui-bg-red layuiadmin-badge">元</span>
                </div>
                <div class="layui-card-body layuiadmin-card-list">
                    <p class="layuiadmin-big-font acounts"style="font-size:25px;color:#595959"></p>
                </div>
            </div>
        </div>
        <div class="layui-col-sm6 layui-col-md4"style="width:26%">
        <div class="layui-card"style="margin-right:5px;background-color:#cffff1;border:1px #88f3c6 solid;border-radius: 5px;">
          <div class="layui-card-header"style="border-bottom:none">
            实际净额
            <span class="layui-badge layui-bg-black layuiadmin-badge">元</span>
          </div>
          <div class="layui-card-body layuiadmin-card-list">
            <p class="layuiadmin-big-font acounts"style="font-size:25px;color:#595959"></p>
          </div>
        </div>
      </div>
    </div>
  </div>



  </div>
  <div id="main" style="width: 600px;height:400px;"></div>


  <input type="hidden" class="store_id">
  <input type="hidden" class="user_id">

  <input type="hidden" class="ways_source_id">
  <input type="hidden" class="company_id">

  <input type="hidden" class="starttime"><!-- 今天的开始时间 -->
  <input type="hidden" class="endtime"><!-- 今天的开始时间 -->

  <input type="hidden" class="starttimeY"><!-- 昨天的开始时间 -->
  <input type="hidden" class="endtimeY"><!-- 昨天的结束时间 -->

<script src="<?php echo e(asset('/layuiadmin/layui/layui.js')); ?>"></script>
<script>
    var token = sessionStorage.getItem("Usertoken");
    var agentName = sessionStorage.getItem("duizhang_agentName");
    var str=location.search;
    // var user_id=str.split('?')[1];
    // var user_name=str.split('?')[1];
    var user_id="<?php echo e($_GET['user_id']); ?>";
    var user_name="<?php echo e($_GET['user_name']); ?>";
    console.log(user_id)
    console.log(user_name)



    layui.config({
      base: '../../layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index' //主入口模块
    }).use(['index','form','table','laydate'], function(){
        var $ = layui.$
            ,admin = layui.admin
            ,form = layui.form
            ,table = layui.table
            ,laydate = layui.laydate;


      // 未登录,跳转登录页面
      // $(document).ready(function(){
      //     if(token==null){
      //         window.location.href="<?php echo e(url('/mb/login')); ?>";
      //     }
      // })
    // 获取时间
    var nowdate = new Date();
    // 本月
    var year=nowdate.getFullYear();
    var mounth=nowdate.getMonth()+1;
    var day=nowdate.getDate();
    var hour = nowdate.getHours();
    var min = nowdate.getMinutes();
    var sec = nowdate.getSeconds();
    if(mounth.toString().length<2 && day.toString().length<2){
        var nwedata = year+'-0'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
    }
    else if(mounth.toString().length<2){
        var nwedata = year+'-0'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
    }
    else if(day.toString().length<2){
        var nwedata = year+'-'+mounth+'-0'+day+' '+hour+':'+min+':'+sec;
    }
    else{
        var nwedata = year+'-'+mounth+'-'+day+' '+hour+':'+min+':'+sec;
    }
    $('.end-item').val(nwedata);//今天的时间
    $('.endtime').val(nwedata)
    //今天的开始时间
    if(mounth.toString().length<2 && day.toString().length<2){
        var nwedatastart = year+'-0'+mounth+'-0'+day+' '+'00'+':'+'00'+':'+'00';
    }
    else if(mounth.toString().length<2){
        var nwedatastart = year+'-0'+mounth+'-'+day+' '+'00'+':'+'00'+':'+'00';
    }
    else if(day.toString().length<2){
        var nwedatastart = year+'-'+mounth+'-0'+day+' '+'00'+':'+'00'+':'+'00';
    }
    else{
        var nwedatastart = year+'-'+mounth+'-'+day+' '+'00'+':'+'00'+':'+'00';
    }
    $('.starttime').val(nwedatastart);
    // *******************************************************************************
    var years=nowdate.getFullYear();
    var mounths=nowdate.getMonth()+1;
    var days=nowdate.getDate()-1;
    //昨天的开始时间
    if(mounth.toString().length<2 && day.toString().length<2){
        var yesterdaystart = years+'-0'+mounths+'-0'+days+' '+'00'+':'+'00'+':'+'00';
    }
    else if(mounth.toString().length<2){
        var yesterdaystart = year+'-0'+mounths+'-'+days+' '+'00'+':'+'00'+':'+'00';
    }
    else if(day.toString().length<2){
        var yesterdaystart = years+'-'+mounths+'-0'+days+' '+'00'+':'+'00'+':'+'00';
    }
    else{
        var yesterdaystart = years+'-'+mounths+'-'+days+' '+'00'+':'+'00'+':'+'00';
    }

    if(mounth.toString().length<2 && day.toString().length<2){
        var yesterdayend = years+'-0'+mounths+'-0'+days+' '+'23'+':'+'59'+':'+'59';
    }
    else if(mounth.toString().length<2){
        var yesterdayend = years+'-0'+mounths+'-'+days+' '+'23'+':'+'59'+':'+'59';
    }
    else if(day.toString().length<2){
        var yesterdayend = years+'-'+mounths+'-0'+days+' '+'23'+':'+'59'+':'+'59';
    }
    else{
        var yesterdayend = years+'-'+mounths+'-'+days+' '+'23'+':'+'59'+':'+'59';
    }
    $('.starttimeY').val(yesterdaystart);
    $('.endtimeY').val(yesterdayend);


    // 华丽的分割线----------------------------------------------------
    // nowdate.setMonth(nowdate.getMonth()-1);
    // 上个月
    var y = nowdate.getFullYear();
    var mon = nowdate.getMonth()+1;
    var d = nowdate.getDate();
    var h = '00';
    var m = '00';
    var s = '00';
    if(mon.toString().length<2 && d.toString().length<2){
        var formatwdate = y+'-0'+mon+'-0'+d+' '+h+':'+m+':'+s;
    }
    else if(mon.toString().length<2){
        var formatwdate = y+'-0'+mon+'-'+d+' '+h+':'+m+':'+s;
    }
    else if(d.toString().length<2){
        var formatwdate = y+'-'+mon+'-0'+d+' '+h+':'+m+':'+s;
    }
    else{
        var formatwdate = y+'-'+mon+'-'+d+' '+h+':'+m+':'+s;
    }
    $('.start-item').val(formatwdate);

    if(user_id == ''){
      // $('.layui-card-header').html('对账统计');
    }else{
      $('.user_id').val(user_id)
      $('.transfer').val(user_name)
      $('.a_name').html(user_name);
      acount()
    }


    function acount(){
      // 对账查询
      $.post("<?php echo e(url('/api/user/order_count')); ?>",
      {
        token:token,
        store_id:$('.store_id').val(),
        user_id:$('.user_id').val(),

        time_start:$('.start-item').val(),
        time_end:$('.end-item').val(),

        ways_source:$('.ways_source_id').val(),
        company:$('.company_id').val(),

        amount_start:$('.amount_start').val(),
        amount_end:$('.amount_end').val(),
      },
      function(res){
        console.log(res);
        if(res.status==1){
          $('.layui-col-space15 .layui-col-sm6').eq(0).find('.layui-card .acounts').html(res.data.get_amount);
          $('.layui-col-space15 .layui-col-sm6').eq(1).find('.layui-card .acounts').html(res.data.mdiscount_amount);
          $('.layui-col-space15 .layui-col-sm6').eq(2).find('.layui-card .acounts').html(res.data.total_amount+'/'+res.data.total_count);
          $('.layui-col-space15 .layui-col-sm6').eq(3).find('.layui-card .acounts').html(res.data.refund_amount+'/'+res.data.refund_count);
          $('.layui-col-space15 .layui-col-sm6').eq(4).find('.layui-card .acounts').html(res.data.fee_amount);
          $('.layui-col-space15 .layui-col-sm6').eq(5).find('.layui-card .acounts').html(res.data.member_total_amount);
          $('.layui-col-space15 .layui-col-sm6').eq(6).find('.layui-card .acounts').html(res.data.receipt_amount);
        }else{
          layer.msg(res.message, {
            offset: '50px'
            ,icon: 2
            ,time: 3000
          });
        }

      },"json");

    }



    // 选择门店
    $.ajax({
      url : "<?php echo e(url('/api/user/store_lists')); ?>",
      data : {token:token,l:100},
      type : 'post',
      success : function(data) {
          console.log(data);
          var optionStr = "";
              for(var i=0;i<data.data.length;i++){
                  optionStr += "<option value='" + data.data[i].store_id + "'>" + data.data[i].store_name + "</option>";
              }
              $("#schooltype").append('<option value="">选择门店</option>'+optionStr);
              layui.form.render('select');
      },
      error : function(data) {
          alert('查找板块报错');
      }
    });
    // 选择通道
    $.ajax({
        url : "<?php echo e(url('/api/user/store_open_pay_way_lists')); ?>",
        data : {token:token,l:100},
        type : 'post',
        dataType:'json',
        success : function(data) {
            console.log(data);
            var optionStr = "";
                for(var i=0;i<data.data.length;i++){
                    optionStr += "<option value='" + data.data[i].company + "'>"
                        + data.data[i].company_desc + "</option>";
                }
                $("#passway").append('<option value="">选择通道类型</option>'+optionStr);
                layui.form.render('select');
        },
        error : function(data) {
            alert('查找板块报错');
        }
    });


    // 业务员
    $(".transfer").bind("input propertychange",function(event){
    //    console.log($(this).val())
        user_name = $(this).val()
            if (user_name.length == 0) {
                $('.userbox').html('')
                $('.userbox').hide()
            } else {
        $.post("<?php echo e(url('/api/user/get_sub_users')); ?>",
        {
            token:token,
            user_name:$(this).val(),
            self:'1'

        },function(res){
            console.log(res);
            var html="";
            console.log(res.t)
            if(res.t==0){
                $('.userbox').html('')
            }else{
                for(var i=0;i<res.data.length;i++){
                    html+='<div class="list" data='+res.data[i].id+'>'+res.data[i].name+'-'+res.data[i].level_name+'</div>'
                }
                $(".userbox").show()
                $('.userbox').html('')
                $('.userbox').append(html)
            }

        },"json");
    }
    });

    $(".userbox").on("click",".list",function(){

      $('.transfer').val($(this).html())
      $('.user_id').val($(this).attr('data'))
      $('.userbox').hide()

      acount();

    })
    //  $(".transfer").blur(function(){
//           $('.userbox').hide()
        // });


    // 门店
    $(".inputstore").bind("input propertychange",function(event){
        //  console.log($(this).val())
        store_name = $(this).val()
            if (store_name.length == 0) {
                $('.storebox').html('')
                $('.storebox').hide()
            } else {
          $.post("<?php echo e(url('/api/user/store_lists')); ?>",
          {
              token:token,
              token:token,store_name:$(this).val(),l:100

          },function(res){
              console.log(res);
              var html="";
              console.log(res.t)
              if(res.t==0){
                  $('.storebox').html('')
              }else{
                  for(var i=0;i<res.data.length;i++){
                      html+='<div class="list" data='+res.data[i].store_id+'>'+res.data[i].store_name+'</div>'
                  }
                  $(".storebox").show()
                  $('.storebox').html('')
                  $('.storebox').append(html)
              }

          },"json");
        }
    });


    $(".storebox").on("click",".list",function(){

      $('.inputstore').val($(this).html())
      $('.store_id').val($(this).attr('data'))
      $('.storebox').hide()

      $("#passway").html('')

      acount();

      // 选择通道
      $.ajax({
          url : "<?php echo e(url('/api/user/store_open_pay_way_lists')); ?>",
          data : {token:token,l:100,store_id:$('.store_id').val()},
          type : 'post',
          dataType:'json',
          success : function(data) {
              console.log(data);
              var optionStr = "";
                  for(var i=0;i<data.data.length;i++){
                      optionStr += "<option value='" + data.data[i].company + "'>"
                          + data.data[i].company_desc + "</option>";
                  }
                  $("#passway").append('<option value="">选择通道类型</option>'+optionStr);
                  layui.form.render('select');
          },
          error : function(data) {
              alert('查找板块报错');
          }
      });

    })




    // 选择业务员
    form.on('select(agent)', function(data){
      var user_id = data.value;
      $('.user_id').val(user_id);
      acount();
      if(user_id == ''){
        // 选择门店
        $.ajax({
            url : "<?php echo e(url('/api/user/store_lists')); ?>",
            data : {token:token,l:100},
            type : 'post',
            success : function(data) {
                console.log(data);
                var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].store_id + "'>"
                          + data.data[i].store_name + "</option>";
                    }
                    $("#schooltype").html('');
                    $("#schooltype").append('<option value="">选择门店</option>'+optionStr);
                    layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });
      }else{
        // 选择门店
        $.ajax({
            url : "<?php echo e(url('/api/user/store_lists')); ?>",
            data : {token:token,user_id:user_id,l:100},
            type : 'post',
            success : function(data) {
                console.log(data);
                var optionStr = "";
                    for(var i=0;i<data.data.length;i++){
                        optionStr += "<option value='" + data.data[i].store_id + "'>"
                          + data.data[i].store_name + "</option>";
                    }
                    $("#schooltype").html('');
                    $("#schooltype").append('<option value="">选择门店</option>'+optionStr);
                    layui.form.render('select');
            },
            error : function(data) {
                alert('查找板块报错');
            }
        });

      }

    });
    // 选择门店
    form.on('select(schooltype)', function(data){
      var store_id = data.value;
      $('.store_id').val(store_id);
      //执行重载
      acount()
    });

    // 选择支付类型
    form.on('select(status)', function(data){
      var store_id = data.value;
      $('.ways_source_id').val(store_id);
      //执行重载
      acount()
    });
    // 选择通道类型
    form.on('select(passway)', function(data){
      var store_id = data.value;
      $('.company_id').val(store_id);
      //执行重载
      acount()
    });

    laydate.render({
      elem: '.start-item'
      ,type: 'datetime'
      ,trigger: 'click'
      ,done: function(value){
        $('.start-item').val(value)
        //执行重载
        acount();
      }
    });

    laydate.render({
      elem: '.end-item'
      ,trigger: 'click'
      ,type: 'datetime'
      ,done: function(value){
        $('.end-item').val(value)
        //执行重载
        acount();
      }
    });

    // 开始金额-结束金额****
    $('.amount_start').bind("input propertychange",function(event){
      console.log($(this).val())

      var amount_start=parseInt($(this).val())
      var amount_end=parseInt($('.amount_end').val())
      console.log()
      if(amount_start<amount_end){//开始金额
        acount();
      }else if($('.amount_end').val() == ''){//结束金额

      }else{
        // layer.msg('开始金额不能大于结束金额')
      }

    });
    $('.amount_end').bind("input propertychange",function(event){
      console.log($(this).val())
      var amount_start=parseInt($('.amount_start').val())
      var amount_end=parseInt($(this).val())
      if(amount_start<amount_end){//开始金额
        acount();
      }else if($('.amount_start').val() == ''){//结束金额

      }else{//结束金额
        // layer.msg('开始金额不能大于结束金额')
      }

    });
    // *******

    $('#today').click(function(){
      $('.start-item').val($('.starttime').val())
      $('.end-item').val($('.endtime').val());
      acount();
    })

    $('#yesterday').click(function(){
      $('.start-item').val($('.starttimeY').val())
      $('.end-item').val($('.endtimeY').val())
      acount();
    })

    acount();



    });

  </script>

</body>
</html>